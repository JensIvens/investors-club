<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'become' => 'become',
    'an' => 'an',
    'investor' => 'investor',
    'register' => 'REGISTER!',
    'registertitle' => 'Register today!',
    'registersubtitle' => "Interested in the Angel.me Investors Club? Leave us your emailaddress and we get back to you, very soon, with more information.",
    'subtitle' => 'with the Investors Club!',    
    'join' => 'Join now!',
    'taxshelter' => 'Tax Shelter for startups',
    'investorsclub' => 'Investors Club',
    'invest' => 'invest...',
    'intro1' => 'Did you always dreamed about becoming an investors and invest in promising startups? Great, join the Investors Club and start your investors career right now.',
    'intro2' => "We think that everyone deserves the chance to invest in promising startups. Whether you're an experienced investors or not. The usability of the Investors Club linked with interesting opportunities like the Belgian ",
    'intro3' => ", makes it more interesting than ever before to start your investors career today!",
    'intro4' => 'What are you waiting for? Join now and start your career as an investor today. Or add your company details so we can invite you for some coffee to see if we can do something for you.',
    'howto' => 'How does it work?',
    'iwant' => 'I want to', 
    'isearch' => "I'm looking for ",
    'capital' => "investments...",
    'howto1' => 'Join the Investors Club right now.',
    'howto2' => 'Discover interesting investment-possibilities and dive into their businessplan.',
    'howto3' => 'Invest in startups and benefit from the Belgian Tax Shelter',
    'howto4' => 'Join the Investors Club and create your business page.',
    'howto5' => "Plans to raise money in the future? Great, let's talk!",
    'howto6' => 'Start an investment round on the Angel.me Investors Club and raise your desired amount of money in exchange for equity.',
    'goal' => "It's our goal to let you <strong>invest successfully</strong>",
    'goalsubtitle' => 'Do you wonder how <strong>Angel.me</strong> can help you?',
    'goalsummary' => 'The <strong>Investors Club</strong> tries to make every investment a succesfull story for everyone.',
    'goal1' => 'We bring investors and promising startups together via our online platform. Everyone can join, without any obligation. Also every company can join the Investors Club, also with no obligation at all!',
    'goal2' => "Investing is something that you don't do on your own. Invest together with hundreds experienced investors. No skyhigh hidden costs, but very clear fees and no opportunities for every budget. You can start investing at €1.000,-",
    'goal3' => "Every member can create his own investors profile. It gives us the chance to send you personalized investment opportunities",
    'network' => 'Angel.me network',
    'networkintro' => 'Start your business with crowdfunding and grow with the Investors Club',
    'networktext' => "<p>A couple of years ago, we noticed that startup founders had difficulties to raise capital to start and grow their business. Entrepreneurs enough, but most of them didn’t had the financial resources to start. With our <a href='http://angel.me' target='_blank'>Angel.me Crowdfunding platform</a> we tried to solve that problem. Ambitious entrepreneurs have now the opportunity to use the crowd if they think about starting their own business. The first important step to build their dream was taken. </p><p>Once the entrepreneurs got started, quite often they need more money to grow their business. That’s the second problem we’re going to solve with our Angel.me Investors Club. Private investors can now use our service to invest in promising companies, in exchange for equity. The new ‘Tax Shelter for startups’ (BE only) makes it even more interesting, because every investors gets a tax reduction of up to 45% for ‘every’ investment he makes.</p>",
];
