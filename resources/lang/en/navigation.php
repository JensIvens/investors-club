<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'investorsclub' => 'Investors Club',
    'intro' => ', your career as an investors starts here!',
    'invest' => 'Invest',
    'companies' => 'For companies',
    'join' => 'Join now!',
    'register' => 'Register',
    'profile' => 'Profile',
    'login' => 'Login',
];
