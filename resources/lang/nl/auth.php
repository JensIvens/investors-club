<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'Emailadres en wachtwoord komen niet overeen.',
    'throttle' => 'Sorry, je had teveel pogingen nodig om in te loggen. Probeer opnieuw binnen :seconds seconden.',

];
