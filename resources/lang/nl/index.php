<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'become' => 'word',
    'an' => '',
    'investor' => 'investeerder',
    'subtitle' => 'met de Investors Club!',
    'join' => 'Word lid!',
    'invest' => 'investeren...',
    'register' => 'REGISTREER!',
    'registertitle' => 'Registreer je nu!',
    'registersubtitle' => "Do you think the Investors Club is something for you? Leave us your emailaddress and we get back to you soon with more info!",
    'iwant' => 'Ik wil', 
    'investorsclub' => 'Investeerders Club',
    'taxshelter' => 'Tax Shelter voor startende ondernemingen',
    'intro1' => 'Heb jij altijd al willen investeren in beloftevolle bedrijven, maar weet je niet goed hoe je er moet aan beginnen? Super, dan ben je bij ons aan het juiste adres.',
    'intro2' => "Wij vinden dat iedereen de kans verdient om te kunnen investeren in jonge bedrijven. Of je nu ervaring hebt als investeerder of juist niet. Het gebruiksgemak van de Investors Club gekoppeld aan interessante maatregelen zoals de",
    'intro3' => ", maakt het interessanter dan ooit om jouw carrière als investeerder vandaag nog te starten!",
    'intro4' => 'Waar wacht je nog op? Word lid en start jouw carrière als investeerder vandaag nog. Of voeg jouw bedrijf toe en kom bij ons op de koffie om te kijken wat we samen kunnen doen.',
    'howto' => 'Hoe werkt het?',
    'isearch' => "Ik zoek",
    'capital' => "kapitaal...",
    'howto1' => 'Word lid van onze Investors Club.',
    'howto2' => 'Ontdek interessante investeringsmogelijkheden en duik in het businessplan.',
    'howto3' => 'Investeer in jonge bedrijven en geniet van de voordelen van de Tax Shelter',
    'howto4' => 'Word gratis lid van onze Investors Club en voeg je bedrijfspagina toe.',
    'howto5' => "Ben je op zoek naar investeringskapitaal? Great, let's talk!",
    'howto6' => 'Start een investeringsronde via de Angel.me Investors Club en haal het gewenste kapitaal op in ruil voor aandelen.',
    'goal' => "<strong>Succesvol</strong> investeren is ons doel!",
    'goalsubtitle' => "Hoe pakken we dat aan bij <strong>Angel.me</strong>?",
    'goalsummary' => 'De <strong>Investors Club</strong> probeert iedere investering om te vormen in een succesvol verhaal voor alle partijen. Dit proberen we op verschillende manieren:',
    'goal1' => 'Wij brengen (potentiële) investeerders en interessante ondernemingen samen. Iedereen kan lid worden, zonder enige verplichting. Ook iedere ondernemer die investeringskapitaal zoekt kan lid worden, ook hier zonder enige verplichting.',
    'goal2' => 'Investeren doe je niet alleen. Samen met honderden ervaren investeerders kan je zelfs beginnen met het kleinste bedrag. Geen torenhoge kosten, iedereen die wil investeren kan dat, al vanaf € 1.000,-',
    'goal3' => "Iedereen die lid wordt kan een investeringsprofiel opstellen. Dankzij jouw gepersonaliseerd profiel kan je makkelijk interessante investeringsmogelijkheden ontdekken op jouw maat.",
    'network' => 'Angel.me netwerk',
    'networkintro' => 'Start jouw bedrijf met crowdfunding en groei via de Investors Club',
    'networktext' => "<p>Enkele jaren geleden merkten wij dat het voor beginnende ondernemers moeilijk was om startkapitaal op te halen. De ondernemingszin was wel aanwezig, maar ondernemers hadden de financiële middelen niet om te starten. Via het <a href='http://angel.me' target='_blank'>Angel.me crowdfunding-platform</a> hebben we getracht dat probleem op te lossen. Gemotiveerde ondernemers kunnen nu bij de 'crowd' geld ophalen om de eerste belangrijke stap in de ondernemerswereld te zetten.
        </p>
        <p>Eens de ondernemers gestart zijn hebben ze al snel meer kapitaal nodig om te kunnen groeien. Via de Angel.me Investors Club willen we dat probleem oplossen. Particulieren kunnen via ons investeren in beloftevolle bedrijven, in ruil voor aandelen. <a href='{{ url('/taxshelter') }}'>De nieuwe Tax Shelter</a> maakt het extra interessant, want je krijgt als investeerder tot 45% van het geïnvesteerde bedrag terug als belastingsvermindering. 
            </p>",
];
