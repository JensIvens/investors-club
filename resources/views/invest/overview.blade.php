@extends('layouts.app')

@section('content')
<section class="projectsoverview">
	<div class="header">
		<div class="title">
			<h1>Belgian Chocolate Skinfood</h1>
			<h2>Aanraken is een levensbehoefte!</h2>
			<ul>
				<li><a href="#"><i class="fa fa-globe" aria-hidden="true"></i></a></li>
				<li><a href="#"><i class="fa fa-facebook-square" aria-hidden="true"></i></a></li>
				<li><a href="#"><i class="fa fa-twitter-square" aria-hidden="true"></i></a></li>

			</ul>
		</div>
		<iframe src="https://www.youtube.com/embed/qPJrpkuAxjQ" frameborder="0" allowfullscreen></iframe>
	</div> 
		<nav>
	        <ul class="nav nav-tabs">
				<li role="presentation" class="active">
					<a href="#" class="personal">Pitch</a>
				</li>
				<li>
					<a href="#">Team</a>
				</li>
				<li>
					<a href="#">Market</a>
				</li>
				<li>
					<a href="#">Docs</a>
				</li>
	            <button src="#" class="btn btn-default-inverse pull-right">Back this project!</button>
	        </ul>
		</nav>
	<div class="row">
		<div class="col-md-8">
			<h3>Belgian Chocolate Skinfood</h3>
			<p>
				In één week Zweden van Noord naar Zuid per fiets doorkruisen. Met 2100 kilometer is dit niet een typisch fietstochtje. Aan de 'Length of Sweden' is sowieso weinig 'typisch'. Elke verwachting die je hebt, kan eigenlijk meteen overboord. Je kunt niet voorbereiden op het onverwachte. Je kunt jezelf er alleen maar aan overgeven.
			</p>
			<p>
				In één week Zweden van Noord naar Zuid per fiets doorkruisen. Met 2100 kilometer is dit niet een typisch fietstochtje. Aan de 'Length of Sweden' is sowieso weinig 'typisch'. Elke verwachting die je hebt, kan eigenlijk meteen overboord. Je kunt niet voorbereiden op het onverwachte. Je kunt jezelf er alleen maar aan overgeven.
			</p>
			<img src="\images/1334.jpg" alt="Banner image">
			<p>
				Vol trots presenteren we 'Length of Sweden', een documentaire van Ertzui Film met in de hoofdrol Rita Jett, Kristian Hallberg, Erik Nohlin en de Sequoia.
			</p>
			<blockquote>
				<p>"TIJDENS HET RIJDEN VAN LANGE AFSTANDEN ONDERGA JE EEN SOORT GOLFBEWEGING. OP DE TOP VAN DE GOLF VOEL JE JE ONVERSLAANBAAR EN LOOPT ALLES OP ROLLETJES. IN HET DAL VAN DE GOLF, ALS JE VERGEET TE DRINKEN OF WANNEER ER EEN FIKSE TEGENWIND OPSTEEKT, DAN IS HET AFZIEN BIJ DE BEESTEN."</p>
				<footer>Erik Nohlin</footer>
			</blockquote>

			<h4>Subtitle</h4>
			<p>
				Erik stuurde mij een paar maanden geleden een email: "Ik heb een gestoord idee. Ga mee naar Zweden en kom 2100km fietsen - in één week!" Ik het er eigenlijk niet echt over nagedacht toen ik antwoordde, "Ja, dat lijkt mij wel een goed idee!"
			</p>
			<img src="\images/1330.jpg" alt="Banner image">
			<h3>Length of Sweden</h3>
			<p>
				In één week Zweden van Noord naar Zuid per fiets doorkruisen. Met 2100 kilometer is dit niet een typisch fietstochtje. Aan de 'Length of Sweden' is sowieso weinig 'typisch'. <a href="">Elke verwachting die je hebt, kan eigenlijk meteen overboord.</a> Je kunt niet voorbereiden op het onverwachte. Je kunt jezelf er alleen maar aan overgeven.
			</p>
			<p>
				Vol trots presenteren we 'Length of Sweden', een documentaire van Ertzui Film met in de hoofdrol Rita Jett, Kristian Hallberg, Erik Nohlin en de Sequoia.
			</p>
			<p>
				Erik stuurde mij een paar maanden geleden een email: "Ik heb een gestoord idee. Ga mee naar Zweden en kom 2100km fietsen - in één week!" Ik het er eigenlijk niet echt over nagedacht toen ik antwoordde, "Ja, dat lijkt mij wel een goed idee!"
			</p>
			<iframe src="https://www.youtube.com/embed/qPJrpkuAxjQ" frameborder="0" allowfullscreen></iframe>
			<p>
				Vol trots presenteren we 'Length of Sweden', een documentaire van Ertzui Film met in de hoofdrol Rita Jett, Kristian Hallberg, Erik Nohlin en de Sequoia.
			</p>
			<p>
				Erik stuurde mij een paar maanden geleden een email: "Ik heb een gestoord idee. Ga mee naar Zweden en kom 2100km fietsen - in één week!" Ik het er eigenlijk niet echt over nagedacht toen ik antwoordde, "Ja, dat lijkt mij wel een goed idee!"
			</p>
		</div>
		<div class="col-md-4 financing">
			<div class="row">
				<div class="col-md-6">
					<span>Investment sought:</span>
					<p>€ 2.500.000,-</p>
				</div>
				<div class="col-md-6">
					<span>Equity offered:</span>
					<p>9,52 %</p>
				</div>
			</div>
			<div class="row">
				<div class="col-md-6">
					<span>Investment amount:</span>
					<p>€ 2.500.000,-</p>
				</div>
				<div class="col-md-6">
					<span>Pre-money valuation:</span>
					<p>€ 6.521.879,-</p>
				</div>
			</div>
		</div>
	</div>
</section>

@endsection

@section('scripts')
<script>
	var  nav = $(".projectsoverview nav");
    mns = "navbar-fixed-top";
    hdr = $('.header').height() + 107;

$(window).scroll(function() {
  if( $(this).scrollTop() > hdr ) {
    nav.addClass(mns);
  } else {
    nav.removeClass(mns);
  }
});

</script>

@endsection