<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="keywords" content="">
    <meta name="description" content="Angel.me Investors Club">
    <meta name="author" content="Angel.me">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!-- FB open graph -->
    <meta property="fb:app_id"          content="362500034083606" /> 
    <meta property="og:url"                content="http://investorsclub.jensivens.be/en" />
    <meta property="og:type"               content="website" />
    <meta property="og:title"              content="Investors Club - Your career as an investor starts here!" />
    <meta property="og:description"        content="Did you always dreamed about becoming an investors and invest in promising startups? We believe that everyone deserves that chance, whether you're an experienced investors or not. Join the Investors Club and start your investors career right now." />
    <meta property="og:image"              content="http://investorsclub.jensivens.be/images/facebook_viral.jpg" />

    <!-- TW cards-->
    <meta name="twitter:card" content="summary_large_image">
    <meta name="twitter:site" content="@angeldotme">
    <meta name="twitter:creator" content="@JensIvens">
    <meta name="twitter:title" content="Investors Club - Your career as an investor starts here!">
    <meta name="twitter:description" content="Did you always dreamed about becoming an investors and invest in promising startups? We believe that everyone deserves that chance, whether you're an experienced investors or not. Join the Investors Club and start your investors career right now.">
    <meta name="twitter:image" content="http://investorsclub.jensivens.be/images/twitter_viral.png">


    <title>{{ config('app.name', 'Angel.me Investors Club') }}</title>
    
    <link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/smoothness/jquery-ui.css">
    <!-- STYLESHEETS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
    <link href='https://fonts.googleapis.com/css?family=Raleway:400,100,200,300,700' rel='stylesheet' type='text/css'>  
    <link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/smoothness/jquery-ui.css">

    <link href="{!! asset('css/app.css') !!}" rel="stylesheet">
    
    <!-- HTML5 shiv and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <!-- GOOGLE ANALYTICS -->
    <script type="text/javascript">
      var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
      document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
    </script>
    <script type="text/javascript">
      try{
      var pageTracker = _gat._getTracker("UA-53879290-6");
      pageTracker._trackPageview();
      } catch(err) {}
    </script>
</head>
<body>
<div class="page-wrap" id="app">

    @include('partials.navigation')
    @yield('headerimage')

    <main role="main">

        @yield('content')

    </main>
    
</div>
@include('partials.footer')

     <!-- JAVASCRIPT/jQUERY -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>

    
    <!-- BOOTSTRAP JS -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
    <script>
         $(document).ready(function(){
            $('.dropdown-toggle').dropdown()
        });
    </script>
    <!-- CUSTOM JS SCRIPTS -->
    <script src="{!! asset('js/app.js') !!}"></script>
    <script>
      window.fbAsyncInit = function() {
        FB.init({
          appId      : '362500034083606',
          xfbml      : true,
          version    : 'v2.8'
        });
      };

      (function(d, s, id){
         var js, fjs = d.getElementsByTagName(s)[0];
         if (d.getElementById(id)) {return;}
         js = d.createElement(s); js.id = id;
         js.src = "//connect.facebook.net/en_US/sdk.js";
         fjs.parentNode.insertBefore(js, fjs);
       }(document, 'script', 'facebook-jssdk'));
    </script>
    @yield('scripts')
</body>
</html>
