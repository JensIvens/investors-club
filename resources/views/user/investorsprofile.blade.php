@extends('layouts.app')

@section('content')

@include("partials.dashboard_navigation", ['active' => 'profile'])
    <section class="user-page clearfix">
        <section class="dashboard_content">
            <section class="dashboard_banner">
                <ul class="nav nav-tabs">
                    <li>
                        <a href="{{ url('/dashboard/profile')}}" class="personal">Personal profile</a>
                    </li>
                    <li class="active">
                        <a href="{{ url('/dashboard/investorsprofile')}}" class="investors">Investors profile</a>
                    </li>
                </ul>
            </section>
            <section id="edit-profile" class="dashboard_content_item">  
                <span class="dashboard_title_area">Investeerders profiel <i class="fa fa-angle-down pull-right" aria-hidden="true"></i></span>
                {!! Form::open(array('url' => '/dashboard/investorsprofile', 'method' => 'POST', 'id' => 'submitinvestorsprofile'))!!}
                {{ csrf_field() }}
                <div class="row">                  
                     <div class="clearfix form-group col-md-6">
                        {!! Form::label('experience', 'Heb je ervaring als investeerder?', ['class' => '']) !!}
                        {!! Form::select('experience', ['Yes' => "Ja, ik heb al eerder geïnvesteerd in startups", 'No' => 'Nee, ik heb nog nooit geïnvesteerd'], $investorsprofile->experience, ['placeholder' => 'Heb jij ervaring als investeerder?', 'class' => 'form-control']); !!}
                    </div>
                    <div class="clearfix form-group col-md-6">
                        {!! Form::label('risk', "Hoe zou jij jezelf beschrijven?", ['class' => '']) !!}
                        {!! Form::select('risk', ['Low' => "Ik wil investeren, maar met zo weinig mogelijk risico", 'Medium' => 'Ik ben bereid iets meer risico te nemen om een hoger rendement te halen', 'High' => "Ik ben bereid grote risico's te nemen voor een zo hoog mogelijk rendement"], $investorsprofile->risk, ['placeholder' => 'Hoe zou jij jezelf omschrijven?', 'class' => 'form-control']); !!}
                    </div>
                </div>
                    
                    <span class="dashboard_title_area">In detail <i class="fa fa-angle-down pull-right" aria-hidden="true"></i></span>
                    <div class="row">
                        <div class="clearfix form-group col-md-6">
                            {!! Form::label('amount', "Hoeveel denk je te investeren per jaar?", ['class' => '']) !!}
                            {!! Form::select('amount', ['Low' => "Minder dan € 5.000,-", 'Medium' => 'Tussen de € 5.000 en de € 10.000', 'High' => "Meer dan € 10.000"], $investorsprofile->amount, ['placeholder' => 'Ik heb geen idee..', 'class' => 'form-control']); !!}
                        </div>
                         <div class="clearfix form-group col-md-6">
                            {!! Form::label('location', "Waar moet het bedrijf gevestigd zijn?", ['class' => '']) !!}
                            {!! Form::select('location', ['BENELUX' => "België, Nederland & Luxemburg", 'Europe' => 'Heel Europa', 'USA' => "Enkel Amerikaanse startups", "World" => "Ik wil investeren in bedrijven over de hele wereld"], $investorsprofile->location, ['placeholder' => 'Ik heb geen idee..', 'class' => 'form-control']); !!}
                        </div>
                        <div class="clearfix form-group col-md-6">
                            {!! Form::label('stage', "Welke fase spreekt jou het meest aan?", ['class' => '']) !!}
                            {!! Form::select('stage', ['early' => "Ik wil investeren vanaf de opstart", 'A-round' => 'Ik wil investeren vanaf ze beginnen te groeien', 'grow' => "Ik wil enkel investeren in mature bedrijven die verder willen groeien"], $investorsprofile->stage, ['placeholder' => 'Ik heb geen idee..', 'class' => 'form-control']); !!}
                        </div>
                    </div>
                {!! Form::submit('Opslaan', ['class' => 'btn btn-default pull-right'])!!}
                {!! Form::close() !!}

            </section>
        </section>
    </section>
@endsection
@section('scripts')
    <!-- <script>
        $("#submitpersonalform").submit(function(e){
            e.preventDefault();
            var firstname = "{{Auth::user()->firstname}}";
            var lastname =  "{{Auth::user()->lastname}}";
            var email = "{{Auth::user()->email}}";
            var bio =  "{{Auth::user()->bio}}";
            var public = "{{Auth::user()->public}}";
            var phonenumber = "{{Auth::user()->phonenumber}}";
            var street =  "{{Auth::user()->street}}";
            var number = "{{Auth::user()->number}}";
            var postalcode =  "{{Auth::user()->postalcode}}";
            var city = "{{Auth::user()->city}}";
            var country =  "{{Auth::user()->country}}";
            var dataString = { firstname: firstname, lastname: lastname, email: email, bio: bio, public: public, phonenumber: phonenumber, street: street, number: number, postalcode: postalcode, city: city, country: country  }; 
            $.ajax({
                type: "POST",
                url : "/dashboard/me",
                data : dataString,
                success : function(data){
                    console.log(data);
                    console.log('succes');
                }
            },"json");
    
        });
    </script> -->
@endsection