@extends('layouts.app')

@section('content')

@include("partials.dashboard_navigation", ['active' => 'profile'])
    <section class="user-page clearfix">
        <section class="dashboard_content">
            <section class="dashboard_banner">
                <ul class="nav nav-tabs">
                    <li role="presentation" class="active">
                        <a href="#" class="personal">Personal profile</a>
                    </li>
                    <li>
                        <a href="{{ url('/dashboard/investorsprofile')}}" class="investors">Investors profile</a>
                    </li>
                </ul>
            </section>
            <div class="tab-content">
                <div role="tabpanel" class="tab-pane fade in active" id="personal">
                    <section id="edit-profile" class="dashboard_content_item">  
                        <span class="dashboard_title_area">Personal information <i class="fa fa-angle-down pull-right" aria-hidden="true"></i></span>
                        {!! Form::open(array('url' => '/dashboard/profile', 'method' => 'POST', 'id' => 'submitpersonalform'))!!}
                            {{ csrf_field() }}
                            <div class="row">
                                <div class="clearfix form-group col-md-6">
                                    {!! Form::label('firstname', 'Voornaam', ['class' => '']) !!}
                                    {!! Form::text('firstname', Auth::User()->firstname , ['class' => 'form-control', 'placeholder' => 'Voornaam', 'required']) !!}
                                </div>
                                <div class="clearfix form-group col-md-6">
                                    {!! Form::label('lastname', 'Achternaam', ['class' => '']) !!}
                                    {!! Form::text('lastname', Auth::User()->lastname , ['class' => 'form-control', 'placeholder' => 'Achternaam', 'required']) !!}
                                </div>
                            </div>
                            <div class="row">
                                <div class="clearfix form-group col-md-8">
                                    {!! Form::label('bio', 'Bio', ['class' => '']) !!}
                                    {!! Form::text('bio', Auth::User()->bio , ['class' => 'form-control', 'placeholder' => 'Describe yourself in 1 line']) !!}
                                </div>
                                <div class="clearfix form-group col-md-4">
                                {!! Form::label('public', 'Public?', ['class' => '']) !!}
                                {!! Form::select('public', ['yes' => 'Show it!', 'no' => "Don't show it!"], Auth::user()->public, ['class' => 'form-control']) !!}
                                </div>
                            </div>
                    </section>
                    <section id="edit-profile" class="dashboard_content_item">  
                        <span class="dashboard_title_area">Contact information <i class="fa fa-angle-down pull-right" aria-hidden="true"></i></span>
                        <div class="row">
                            <div class="clearfix form-group col-md-6">
                                {!! Form::label('email', 'Email*', ['class' => '']) !!}
                                {!! Form::email('email', Auth::User()->email , ['class' => 'form-control', 'placeholder' => 'Email:', 'required']) !!}
                            </div>
                            <div class="clearfix form-group col-md-6">
                                {!! Form::label('phonenumber', 'Phone number', ['class' => '']) !!}
                                {!! Form::text('phonenumber', Auth::User()->phonenumber , ['class' => 'form-control', 'placeholder' => 'Ex. +32 ...']) !!}
                            </div>
                        </div>

                        <div class="row">
                            <div class="clearfix form-group col-md-9">
                                {!! Form::label('street', 'Street', ['class' => '']) !!}
                                {!! Form::text('street', Auth::User()->street , ['class' => 'form-control', 'placeholder' => 'Street']) !!}
                            </div>
                            <div class="clearfix form-group col-md-3">
                                {!! Form::label('number', 'Huisnummer', ['class' => '']) !!}
                                {!! Form::text('number', Auth::User()->number , ['class' => 'form-control', 'placeholder' => 'Ex.4']) !!}
                            </div>
                        </div>
                        <div class="row">
                            <div class="clearfix form-group col-md-2">
                                {!! Form::label('postalcode', 'Postcode', ['class' => '']) !!}
                                {!! Form::text('postalcode', Auth::User()->postalcode , ['class' => 'form-control', 'placeholder' => 'Postal code']) !!}
                            </div>
                            <div class="clearfix form-group col-md-5">
                                {!! Form::label('city', 'City', ['class' => '']) !!}
                                {!! Form::text('city', Auth::User()->city , ['class' => 'form-control', 'placeholder' => 'City']) !!}
                            </div>
                        
                            <div class="clearfix form-group col-md-5">
                                {!! Form::label('country', 'Land', ['class' => '']) !!}
                                {!! Form::text('country', Auth::User()->country , ['class' => 'form-control', 'placeholder' => 'Land']) !!}
                            </div>
                        </div>
                        {!! Form::submit('Bijwerken', ['class' => 'btn btn-default pull-right'])!!}
                        {!! Form::close() !!}
                        </section>
                    </div>
                </div>
        </section>
    </section>
@endsection
@section('scripts')
    <script>
        /*$("#submitpersonalform").submit(function(e){
            e.preventDefault();
            var firstname = "{{Auth::user()->firstname}}";
            var lastname =  "{{Auth::user()->lastname}}";
            var email = "{{Auth::user()->email}}";
            var bio =  "{{Auth::user()->bio}}";
            var public = "{{Auth::user()->public}}";
            var phonenumber = "{{Auth::user()->phonenumber}}";
            var street =  "{{Auth::user()->street}}";
            var number = "{{Auth::user()->number}}";
            var postalcode =  "{{Auth::user()->postalcode}}";
            var city = "{{Auth::user()->city}}";
            var country =  "{{Auth::user()->country}}";
            var dataString = { firstname: firstname, lastname: lastname, email: email, bio: bio, public: public, phonenumber: phonenumber, street: street, number: number, postalcode: postalcode, city: city, country: country  }; 
            $.ajax({
                type: "POST",
                url : "/dashboard/profile",
                data : dataString,
                success : function(data){
                    console.log(data);
                    console.log('succes');
                }
            },"json");
    
        });*/
    </script>
@endsection