@extends('layouts.app')

@section('content')
<header class="front-header clearfix" style="background-image: url('images/main-image.jpg')">
    <h1 class="call-to-shift">
        {{ ucfirst(trans('index.become')) }}<span> {{ trans('index.an') }} <em>{{ trans('index.investor') }}</em></span> 
        <span class="sparkle">{{ trans('index.subtitle') }}</span>
    </h1>
    <div class="action-bar clearfix">
        <ul class="">
            <li><a href="#intro">#investorsclub?</a></li>
            <li><a href="#register">{{ trans('index.join') }}</a></li>
        </ul>
    </div>
</header>
<div class="content">
    <div id="intro">
        <section class="split shift-brand">
            <img src="images/investorsclub.png" alt="Logo Angel.me Investors Club">
        </section> 
        <section class="split storytext">
            <h2>
                {{ trans('index.investorsclub') }}
            </h2>
            <p>
                <strong>
                    {{ trans('index.intro1') }}
                </strong>
            </p>
            <p>
                {{ trans('index.intro2') }} 
                        {{ trans('index.taxshelter') }}
                {{ trans('index.intro3') }}
            </p>

            <p>
                {{ trans('index.intro4') }}
            </p>

            <a href="#register" class="btn btn-default">
                {{ trans('index.join') }}
            </a>
        </section>
    </div>


    <div id="register">
        <section class="split left">
            <h2>
                {!! trans('index.registertitle') !!}
            </h2>
            <p class="text-center">
                {!! trans('index.registersubtitle') !!}
                <i class="fa fa-arrow-right"></i>
            </p>
        </section>
        <section class="split right">
            <div class="form">
                {!! Form::open(array('url' => '/register.php', 'method' => 'POST', 'id' => 'registerform'))!!}
                    {{ csrf_field() }}
                    
                    {!! Form::label('email', 'Your email:') !!}
                    {!! Form::text('email', null, ['id' => 'emailaddress','class' => "input", 'placeholder' => 'Ex. investor@angel.me', 'required']) !!}
                        @if ($errors->has('email'))
                            <span class="help-block">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                        @endif
                        <span id="result"></span>

                    {!! Form::submit(trans('index.register'), ['class' => 'btn btn-default-inverse'])!!}
                {!! Form::close() !!}
            </div>

        </section>
    </div>

    <div id="howto">
        <h2>{{ trans('index.howto') }} <span class="icon fa fa-credit-card"></span></h2>
        <div class="howto huren">
            <section class="left split">
                <h3>
                    {{ trans('index.iwant') }}
                    <br>
                    <strong>
                        {{ trans('index.invest') }}
                    </strong>
                    <i class="fa fa-arrow-right"></i>
                </h3>
            </section>
            <section class="right split">                   
                <ul>
                    <li>
                        <p>
                            <i class="fa fa-check sparkle"></i>
                            {{ trans('index.howto1') }}
                        </p>
                    </li>
                    <li>
                        <p>
                            <i class="fa fa-check sparkle"></i>
                            {{ trans('index.howto2') }}
                        </p>
                    </li>
                    <li>
                        <p>
                            <i class="fa fa-check sparkle"></i>
                            {{ trans('index.howto3') }}
                        </p>
                    </li>
                </ul>
            </section>
        </div>

        <div class="howto">
            <section class="left split">
                <ul>
                    <li>
                        <p>
                            <i class="fa fa-check sparkle"></i>
                            {{ trans('index.howto4')}}
                        </p>
                    </li>
                    <li>
                        <p>
                            <i class="fa fa-check sparkle"></i>
                            {{trans('index.howto5') }}
                        </p>
                    </li>
                    <li>
                        <p>
                            <i class="fa fa-check sparkle"></i>
                            {{trans('index.howto6')}}
                        </p>
                    </li>
                </ul>
            </section>
            <section class="right split">
                <h3>
                    {{ trans('index.isearch') }} 
                    <br>
                    <i class="fa fa-arrow-left"></i>
                    <strong>
                        {{ trans('index.capital') }}
                    </strong>
                </h3>
            </section>
        </div>
        <a href="#register" class="btn btn-default">
            {{trans('index.join') }}
        </a>
    </div>

    <div id="goal">
        <section class="split left">
            <h2>
                {!! trans('index.goal') !!}
            </h2>
            <p class="text-center">
                {!! trans('index.goalsubtitle') !!}
                <i class="fa fa-arrow-right"></i>
            </p>
        </section>
        <section class="split right">
            <p>
                {!! trans('index.goalsummary') !!}
            </p>

            <ul class="safety-measures">
                <li><span class="circle">1</span>
                    <p>
                        {!! trans('index.goal1') !!}
                    </p>
                </li>
                <li><span class="circle">2</span>
                    <p>
                        {!! trans('index.goal2') !!}
                    </p>
                </li>
                <li><span class="circle">3</span>
                    <p>
                        {!! trans('index.goal3') !!}
                    </p>
                </li>
            </ul>
        </section>
    </div>

    <div id="angeldotme">
        <section class="split storytext">
            <h2>
                {{trans('index.network')}}
            </h2>
            <p>
                <strong>
                    {{trans('index.networkintro')}}
                </strong>
            </p>
            {!! trans('index.networktext') !!}
            <a href="#register" class="btn btn-default-inverse">
                {{trans('index.join')}}
            </a>
        </section>

        <section class="split shift-brand">
            <img src="images/investorsclub.png" alt="Logo Angel.me Investors Club">
        </section>
    </div>

</div>
@endsection

@section('scripts')
<script>
$(document).ready(function(){

 // MAILCHIMP SUBMIT FORM
 $('#registerform').submit(function(e) {
            e.preventDefault();

          $.ajax({
            type: "POST",
            url: "/register.php",
            data: {
              email: $('#emailaddress').val()
            },
            success: function(data){
              $('#result').html("Thank you, we keep you posted!").css('color', 'green');
            },
            error: function() {
              $('#result').html('Oops, an error occured, try again later, or send us an email: team@angel.me').css('color', 'red');
            }
          });
      });
});
</script>
@endsection