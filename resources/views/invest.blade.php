@extends('layouts.app')

@section('content')
<header class="front-header clearfix" style="background-image: url('/images/main-image.jpg')">
    <div class="clearfix">
        <h1 class="call-to-shift">INVESTEER</h1>
        <div class="action-bar clearfix">
            <ul class="">
                <li><a href="#intro">Meer info</a></li>
                <li><a href="{{url('/register')}}">Word lid!</a></li>
            </ul>
        </div>
    </div>
</header>
<div class="content">
    <div class="clearfix">
        <div id="intro">
            <section class="split shift-brand">
                <img src="/images/investorsclub.png" alt="">
            </section>
            <section class="split storytext">
                <h2>Investeer</h2>
                <p><strong>Investeer op een makkelijke manier in beloftevolle bedrijven!</strong></p>
                <p>Wij vinden dat iedereen de kans verdient om te kunnen investeren in interessante ondernemingen. Met de Angel.me Investors Club kan dat! Wij zorgen ervoor dat je op een heel makkelijke manier kan investeren in interessante bedrijven.
                </p>
                <p>Onderstaand stappenplan geeft je een idee van het verloop van een investering. Op een zeer makkelijke en intuitieve manier zorgen wij ervoor dat je kan investeren in projecten die op jouw maat gesneden zijn. En dit zowel voor de ervaren investeerder als voor iemand die nog nooit geïnvesteerd heeft.
                </p>
                <a href="{{ url('/register') }}" class="btn btn-default-inverse">WORD LID</a>
            </section>
        </div>
    </div>
        <div id="howto">
        <h2>Hoe werkt het? <span class="icon fa fa-credit-card"></span></h2>
        <div class="howto huren">
            <section class="left split">
                <h3>Ik wil <br><strong>investeren...</strong><i class="fa fa-arrow-right"></i></h3>
            </section>
            <section class="right split">                   
                <ul>
                    <li><p><i class="fa fa-check sparkle"></i>Word gratis lid van onze Investors Club.</p></li>
                    <li><p><i class="fa fa-check sparkle"></i>Ontdek de bedrijven die op zoek zijn naar investeringskapitaal</p></li>
                </ul>
            </section>
        </div>

        <div class="howto">
            <section class="left split">
                <ul>
                    <li><p><i class="fa fa-check sparkle"></i>Verdiep je in hun businessplan. Hoe meer je weet van het bedrijf, hoe beter je je investering kan inschatten.</p></li>
                    <li><p><i class="fa fa-check sparkle"></i><strong>Investeer!</strong> Wanneer je 100% gelooft in het bedrijf, kan je overgaan tot een investering. Je kan al investeren vanaf €1.000,-
                    </p></li>
                </ul>
            </section>
            <section class="right split">
                <h3>Ik <br><i class="fa fa-arrow-left"></i><strong>investeer...</strong></h3>
            </section>
        </div>
        <div class="howto huren">
            <section class="left split">
                <h3>Ik heb <br><strong>geïnvesteerd...</strong><i class="fa fa-arrow-right"></i></h3>
            </section>
            <section class="right split">                   
                <ul>
                    <li><p><i class="fa fa-check sparkle"></i>Wij zorgen voor de verdere afhandeling, we sturen de officiële documenten naar je op en zorgen voor de nodige documenten om te genieten van de <a href="{{ url('/taxshelter') }}">Tax Shelter</a>.
                    </p></li>
                    <li><p><i class="fa fa-check sparkle"></i>Geniet van de belastingsvermindering en je kan je officiëel investeerder noemen in bedrijf X.
                    </p></li>
                </ul>
            </section>
        </div>
        <a href="{{ url('/register') }}" class="btn btn-default">WORD LID!</a>
    </div>
    <div id="goal">
        <section class="split left">
            <h2><strong>Let op!</strong></h2>
            <p class="text-center">Investeren in bedrijven is niet zonder risico<i class="fa fa-arrow-right"></i></p>
        </section>
        <section class="split right storytext">
            <p>Het kan dan wel heel makkelijk zijn om te investeren, uiteraard is het niet zonder risico. Niemand kan je garanderen dat jouw investering succesvol gaat zijn. Het gaat steeds over risicovolle beleggingen en de tijd zal uitwijzen of ze al dan niet succesvol gaan zijn.
            </p><br/><br/>
            <p>Wij, Angel.me, faciliteren enkel de investering en kunnen op geen enkel moment aansprakelijk worden gesteld als een investering fout afloopt. Mocht je hierover verdere vragen hebben, aarzel niet om ons te contacteren via: <a href="MAILTO:team@angel.me">team@angel.me</a>
            </p>
        </section>
    </div>


</div>
@endsection