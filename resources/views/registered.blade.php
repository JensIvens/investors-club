@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Start your investors career right now!</div>

                <div class="panel-body">
                    Welcome to the Investors Club, {{ Auth::user()->firstname }}!

                    <p>Share your registration with your friends and invest together in promising startups:</p>

                    @include('partials.share', ['url' => 'http://investorsclub.angel.me/'])
                </div>
                <a href="{{url('/dashboard')}}" role="button" class="btn btn-default-inverse">Dashboard</a>
            </div>

        </div>
    </div>
</div>

<script src="https://code.jquery.com/jquery-2.2.0.min.js"></script>
<script>

    var popupSize = {
        width: 780,
        height: 550
    };

    $(document).on('click', '.social-buttons > a', function(e){

        var
            verticalPos = Math.floor(($(window).width() - popupSize.width) / 2),
            horisontalPos = Math.floor(($(window).height() - popupSize.height) / 2);

        var popup = window.open($(this).prop('href'), 'social',
            'width='+popupSize.width+',height='+popupSize.height+
            ',left='+verticalPos+',top='+horisontalPos+
            ',location=0,menubar=0,toolbar=0,status=0,scrollbars=1,resizable=1');

        if (popup) {
            popup.focus();
            e.preventDefault();
        }

    });
</script>
@endsection
