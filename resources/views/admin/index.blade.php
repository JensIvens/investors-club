@extends('layouts.app')

@section('content')

@include("partials.dashboard_navigation", ['active' => 'admin'])
<section class="user-page clearfix">
	<section class="dashboard_content">
		<section class="dashboard_banner">
			<ul class="nav nav-tabs">
				<li role="presentation" class="active">
					<a href="#" class="personal">Overzicht</a>
				</li>
				<li>
					<a href="#" class="investors">Gebruikers</a>
				</li>
				<li>
					<a href="#" class="investors">Bedrijven</a>
				</li>
			</ul>
		</section>
<!-- 		<div class="row">
		<div class="col-md-6">
			<canvas id="myChart" width="100%" margin="15px" height="100%"></canvas>
		</div>
	</div>
	 -->	</section>
</section>
@endsection

@section('scripts')

<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.3.0/Chart.bundle.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.3.0/Chart.bundle.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.3.0/Chart.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.3.0/Chart.min.js"></script>
<!-- <script>
    var year = ['2013','2014','2015', '2016'];
    var data_users = {{ $users }};

    var barChartData = {
        labels: year,
        datasets: [{
            label: 'Click',
            backgroundColor: "rgba(220,220,220,0.5)",
            data: data_users
        }]
    };

    window.onload = function() {
        var ctx = $('#myChart');
        window.myBar = new Chart(ctx, {
            type: 'bar',
            data: barChartData,
            options: {
                elements: {
                    rectangle: {
                        borderWidth: 2,
                        borderColor: 'rgb(0, 255, 0)',
                        borderSkipped: 'bottom'
                    }
                },
                responsive: true,
                title: {
                    display: true,
                    text: 'Users'
                }
            }
        });

    };
</script>
 -->
@endsection