@extends('layouts.app')

@section('content')

@include("partials.dashboard_navigation", ['active' => 'overview'])
<section class="user-page clearfix">
<section class="banner">
    <h2>Hey {{Auth::User()->firstname}}, welkom!</h2>
        @if(isset($error))
          <div class="alert alert-danger" role="alert">
            <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
            <span class="sr-only">Error:</span>
            {{$error}}
          </div>
        @endif
  		<div class="row">
  		<div class="col-md-6 orange">
			<img src="/images/investorsclub.png" alt="investorsclub">
  		</div>

  		<div class="col-md-6 storytext align-left">
  			<p><strong>Nog even geduld</strong></p>
  			<p>Het eerste project waarin geïnvesteerd kan worden, komt binnenkort online. Momenteel zijn wij investeerders aan het verzamelen en nieuwe projecten aan het analyseren</p>
        <p>Vul alvast je persoonlijke informatie verder aan.</p>
  		</div>
  	</div>
  	<div class="row">
  		<div class="col-md-6">
  			<h3>Mijn profiel</h3>
  			<p>Vul je persoonlijke informatie verder aan en creëer een persoonlijk investeerders profiel.</p>
  			<a href="{{ url('/dashboard/profile') }}" class="btn btn-default-inverse">Mijn Profiel</a>
        <a href="{{ url('/dashboard/investorsprofile') }}" class="btn btn-default">Investeerdersprofiel</a>
  		</div>

  		<div class="col-md-6 color">
  			<h3>Mijn bedrijven</h3>
  			<p>Voeg je bedrijf toe als je op zoek bent naar extra kapitaal. Wij contacteren je nadien zo snel mogelijk.</p>
  			<a href="{{ url('/dashboard/companies') }}" class="btn btn-default">Bedrijven</a>
  		</div>
  	</div>
    
    <h2>Investeer niet alleen <i class="fa fa-retweet" aria-hidden="true"></i></h2>

    <div class="row">
        <div class="col-md-6 color">
          <h3>Nodig je vrienden uit</h3>

          {!! Form::open(array('url' => '/welcome', 'method' => 'POST', 'id' => 'submitinviteform'))!!}
            {{ csrf_field() }}
            {!! Form::email('email', null, ['class' => "input", 'placeholder' => 'Emailadres']) !!}
            @if ($errors->has('email'))
              <span class="help-block">
                <strong>{{ $errors->first('email') }}</strong>
              </span>
            @endif
            {!! Form::submit('Invite!', ['class' => 'btn btn-default-inverse'])!!}
          {!! Form::close() !!}        
        </div>

        <div class="col-md-6">
          <h3>Deel op sociale media</h3>
            @include('partials.share', ['url' => 'http://investorsclub.angel.me/'])
        </div>
    </div>

</section>
@endsection
@section('scripts')
<script>

    var popupSize = {
        width: 780,
        height: 550
    };

    $(document).on('click', '.social-buttons > a', function(e){

        var
            verticalPos = Math.floor(($(window).width() - popupSize.width) / 2),
            horisontalPos = Math.floor(($(window).height() - popupSize.height) / 2);

        var popup = window.open($(this).prop('href'), 'social',
            'width='+popupSize.width+',height='+popupSize.height+
            ',left='+verticalPos+',top='+horisontalPos+
            ',location=0,menubar=0,toolbar=0,status=0,scrollbars=1,resizable=1');

        if (popup) {
            popup.focus();
            e.preventDefault();
        }

    });
</script>
</section>
@endsection
