@extends('layouts.app')

@section('content')
<header class="front-header clearfix" style="background-image: url('/images/main-image.jpg')">
    <div class="clearfix">
        <h1 class="call-to-shift">
            Wanna grow<span><em>your company?</em></span> 
            <span class="sparkle">We got you covered!</span>
        </h1>
        <div class="action-bar clearfix">
            <ul class="">
                <li><a href="#intro">Meer info</a></li>
                <li><a href="{{url('/register')}}">Word lid!</a></li>
            </ul>
        </div>
    </div>
</header>
<div class="content">
    <div class="clearfix">
        <div id="intro">
            <section class="split shift-brand">
                <img src="/images/investorsclub.png" alt="">
            </section>
            <section class="split storytext">
                <h2>Geld ophalen</h2>
                <p><strong>Investeringskapitaal ophalen is niet eenvoudig. Wij helpen je graag verder</strong></p>
                <p>Iedere onderneming wil groeien, hoe sneller hoe liever. Om dat proces te versnellen is het mogelijk om investeringskapitaal binnen te halen in ruil voor aandelen. Dat klinkt makkelijker dan het in werkelijkheid is, maar wij willen graag helpen in dat hele proces.
                </p>
                <p>De Angel.me Investors Club bestaat uit tal van particuliere investeerders die willen investeren in beloftevolle bedrijven. Ben jij op zoek naar investeerders en extra kapitaal, dan lijkt het ons een goed idee om samen te werken.
                </p>
                <p>Niet iedereen heeft ervaring met geld ophalen en dus voorzien wij begeleiding vanaf stap 1. Het moet zowel voor jou als ondernemer als voor de investeerders een succes worden.</p>
            </section>
        </div>
    </div>
    <div id="howto">
        <h2>Hoe werkt het? <span class="icon fa fa-credit-card"></span></h2>
    </div>
    <div id="goal">
        <section class="split left">
            <h2>Investeringskrediet ophalen bij <strong>het grote publiek</strong>..</h2>
            <p class="text-center">Hoe werkt het juist?<i class="fa fa-arrow-right"></i></p>
        </section>
        <section class="split right">
            <ul class="safety-measures">
                <li><span class="circle">1</span>
                    <p>Word lid van de Angel.me Investors Club en maak een account aan voor jouw bedrijf, geef een uitgebreide uitleg over wat je doet en wat je zoekt.</p>
                </li>
                <li><span class="circle">2</span>
                    <p>Wij nemen contact met je op om te kijken of we je kunnen helpen. We proberen je zo goed mogelijk te begeleiden in jouw zoektocht naar investeringskapitaal.
                    </p>
                </li>
                <li><span class="circle">3</span>
                    <p>Als we voor elkaar iets kunnen betekenen, dan kan je online je campagne starten. Je beschrijft wat je noden zijn en wat je daarvoor kan aanbieden. Eens alles duidelijk is kan je je campagne lanceren!
                    </p>
                </li>
                <li><span class="circle">4</span>
                    <p>Wij zorgen ervoor dat jouw campagne wordt opgepikt door onze leden. Jij deelt jouw campagne met jouw netwerk. Vergis je niet, investeringskapitaal ophalen neemt veel tijd in en de zoektocht naar juiste investeerders is niet altijd even eenvoudig.
                    </p>
                </li>
                <li><span class="circle">5</span>
                    <p>Als een investeerder geïnteresseerd is, gaat hij ongetwijfeld heel jouw businessplan onderzoeken. Als hij interesse heegt kan hij contact met jou opnemen of zelfs direct investeren.
                    </p>
                </li>
                <li><span class="circle">6</span>
                    <p>Na enkele weken of enkele maanden stopt jouw campagne en zorgen wij ervoor dat alle officiële documenten in orde worden gebracht. Jij ontvangt je investeringskapitaal en in ruil voorzie jij X% van de aandelen.
                    </p>
                </li>
            </ul>
        </section>
    </div>
    <div id="goal">
        <section class="split right storytext">
            <p>Klinkt het allemaal zeer interessant en denk je dat wij iets voor jou kunnen betekenen? Super!
            </p><br/><br/>
            <p>Maak snel <a href="{{ url('/register') }}">een persoonlijk account aan</a> en voeg jouw bedrijf toe onder het onderdeel 'Bedrijven'. Op die manier beschikken we over alle nodige informatie en kunnen we uitnodigen voor een koffie, alles zonder enige verplichting. 
            </p><br/><br/>
            <p>Ook bedrijven die niet direct op zoek zijn naar investeringskapitaal kunnen lid worden. Misschien kan het van pas komen in de toekomst.</p>
        </section>
        <section class="split left">
            <h2><strong>Interesse?</strong></h2>
            <p class="text-center">De eerste koffie is op onze kosten ;)<i class="fa fa-coffee"></i></p>
        </section>

    </div>

</div>
@endsection