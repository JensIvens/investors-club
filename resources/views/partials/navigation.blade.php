<nav id="mainnav" class="navbar">
		<section class="top-menu clearfix">
			<div class="clearfix">

			<div class="pull-left">
				<h3 class="site-slogan">
					<strong>{{trans('navigation.investorsclub')}}</strong>
					{{ trans('navigation.intro') }}
				</h3>
			</div>
			<ul class="secundary-menu nav navbar-nav pull-right">
				@if(Auth::user())
					<li class="dropdown user-menu">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="true">
							<span>Hey, {{Auth::user()->firstname }}</span><span class="caret"></span>
							@if(Auth::User()->image)
								<span class="nav-user-picture" style="background-image:url(/{{Auth::user()->image->thumbnail_path}});">
							@else
								<span class="nav-user-picture" style="background-image:url({!! asset('/images/placeholders/user-default.png') !!});">
							@endif
						</a>

						<ul class="dropdown-menu">
							<li><a href="{{url('/dashboard')}}">Dashboard</a></li>
							<li><a href="{{url('/dashboard/profile')}}">{{ trans('navigation.profile') }}</a></li>
							<li><a href="{{url('/dashboard/companies')}}">Your companies</a></li>
							<li class="divider"></li>
			                <li>
								<a href="{{ url('/logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
									
									Logout

	                                <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
	                                	{{ csrf_field() }}
									</form>
								</a>
							</li>
						</ul>
					</li>
				@else
					<li><a href="{{url('/login')}}">{{ trans('navigation.login' )}}</a></li>
					<li><a href="{{url('/register')}}">{{ trans('navigation.register') }}</a></li>
					<li>				
						<div class="dropdown pull-right">
							<button class="dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
			    				{{ strtoupper(LaravelLocalization::getCurrentLocale()) }}
			    				<span class="caret"></span>
			  				</button>
			  				<ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
			    				@foreach(LaravelLocalization::getSupportedLocales() as $localeCode => $properties)
			        				<li><a rel="alternate" hreflang="{{$localeCode}}" href="{{LaravelLocalization::getLocalizedURL($localeCode) }}">{{ $properties['native'] }}</a>
				        			</li>
			    				@endforeach			  
					    	</ul>
						</div>
					</li>
				@endif				
			</ul>
			</div>
		</section>
		
		<div class="clearfix">

			<section class="pull-left">
				<a class="logo-link" href="{{url('/')}}"><img src="/images/logo.png" alt="Investors Club logo"></a>
			</section>
			
			<ul class="main-menu pull-right">
				<li><a href="{{url('/invest')}}">{{ trans('navigation.invest') }}</a></li>
				<li><a href="{{url('/taxshelter')}}">Tax Shelter</a></li>
				<li><a href="{{url('/companies')}}">{{trans('navigation.companies')}}</a></li>
				<li><a href="{{url('/#angeldotme')}}"><span class="sparkle">#</span>angeldotme</a></li>
				<li>
					@if(Auth::check())
						<a class="btn-sparkle" href="{{url('/dashboard')}}">Dashboard</a>
					@else
						<a class="btn-sparkle" href="{{url('/register')}}">{{ trans('navigation.join') }}</a>			
					@endif
					</a>
				</li>
			</ul>
			
			<div class="pull-right" id="hamburger">
				<a href="#"><i class="fa fa-bars" aria-hidden="true"></i></a>
			</div>

		</div>

		<div id="main-nav-mobile">
			<ul class="mobile-menu">
				<li><a href="#">{{ trans('navigation.invest') }}</a></li>
				<li><a href="#">Tax Shelter</a></li>
				<li><a href="#">{{trans('navigation.companies')}}</a></li>
				<li><a href="#"><span class="sparkle">#</span>angeldotme</a></li>
				<li><a href="#">{{ trans('navigation.join') }}</a></li>
			</ul>
		</div>
</nav>