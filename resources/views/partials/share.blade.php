<div class="social-buttons">

    <a type="button" class="btn btn-default btn-lg" href="https://www.facebook.com/sharer/sharer.php?u={{ urlencode($url) }}"
       target="_blank">
      <i class="fa fa-facebook" aria-hidden="true"></i> Facebook
    </a>

  <a href="https://twitter.com/intent/tweet?text=I+just+joined+the+@angeldotme+investors+club&url={{ urlencode($url) }}"
       target="_blank" type="button" class="btn btn-default btn-lg">
    <i class="fa fa-twitter" aria-hidden="true"></i> Twitter
  </a>

  <a href="https://www.linkedin.com/shareArticle?mini=true&url={{ urlencode($url) }}&source=LinkedIn" target="_blank" type="button" class="btn btn-default btn-lg">
    <i class="fa fa-linkedin" aria-hidden="true"></i> LinkedIn
  </a>

</div>