<section id="dashboard-menu">
	<div id="user-info" class="clearfix">
		@if(Auth::User()->image)		
		<img src="/{{Auth::user()->image->thumbnail_path}}" alt="">
		@else
		<img src="{!! asset('/images/placeholders/user-default.png') !!}" alt="">
		@endif

		<h2>{{Auth::user()->firstname}} @if(Auth::user()->email_verification == 1)<i class="fa fa-check" aria-hidden="true" alt="Verified account">@endif</i></h2>

		<h3><a href="{{url('/dashboard/profile')}}">Profiel bewerken</a></h3>

		{!! Form::open(array('url' => '/users/addpicture', 'method' => 'POST', 'files' => true, 'id' => 'profilepicchanger'))!!}
		{{ csrf_field() }}
		{!! Form::file('profilepicture', ['class' => 'input-not-displayed', 'id' => "profilepicture"]) !!}
		{!! Form::label('profilepicture', 'Kies nieuwe foto', ['class' => 'btn btn-small btn-default']) !!}
		{!! Form::close() !!}
    <script>
        document.getElementById("profilepicture").onchange = function() {
            document.getElementById("profilepicchanger").submit();
        };
    </script>

	</div>
	
<!-- 	<hr>

<ul class="dashboard-nav dashboard-invest">
	<li class="clearfix">
		<a class="clearfix" href="">
		<span class="icon"><i class="fa fa-line-chart"></i></span>
		<span class="menu-link">Investeer</span>
		</a>
	</li>
</ul>
 -->
	<hr>

	<ul class="dashboard-nav">
		<li @if($active == 'overview') class="active clearfix" @else class="clearfix" @endif>
			<a class="clearfix" href="{{url('/dashboard')}}">
			<span class="icon"><i class="fa fa-anchor"></i></span>
			<span class="menu-link">Overzicht</span>
			</a>
		</li>
		<li @if($active == 'profile') class="active clearfix" @else class="clearfix" @endif >
			<a class="clearfix" href="{{url('/dashboard/profile')}}">
			<span class="icon"><i class="fa fa-user"></i></span>
			<span class="menu-link">Profiel</span>
			</a>
		</li>
		<li @if($active == 'company') class="active clearfix" @else class="clearfix" @endif >
			<a class="clearfix" href="{{ url('/dashboard/companies') }}">
			<span class="icon"><i class="fa fa-building-o"></i></span>
			<span class="menu-link">Bedrijven</span>
			</a>
		</li>
<!-- 		<li @if($active == 'investments') class="active clearfix" @else class="clearfix" @endif >
			<a class="clearfix" href="">
			<span class="icon"><i class="fa fa-pie-chart"></i></span>
			<span class="menu-link">Investeringen</span>
			</a>
		</li>
		 -->		@if(Auth::user()->admin == '1')
		<li @if($active == 'admin') class="active clearfix" @else class="clearfix" @endif >
			<a class="clearfix" href="{{url('/admin')}}">
			<span class="icon"><i class="fa fa-unlock-alt"></i></span>
			<span class="menu-link">Admin</span>
			</a>
		</li>
		@endif
	</ul>

</section>