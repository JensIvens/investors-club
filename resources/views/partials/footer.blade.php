<footer class="clearfix page-footer">
	
	<div class="social">
		<ul>
			<li>
				<a href="https://facebook.com/angeldotme" target="_blank"><i class="fa fa-facebook-square"></i></a>
			</li>
			<li>
				<a href="https://twitter.com/angeldotme" target="_blank"><i class="fa fa-twitter-square"></i></a>
			</li>
			<li>
				<a href="https://linkedin.com/angeldotme" target="_blank"><i class="fa fa-linkedin-square"></i></a>
			</li>
			<li>
				<a href="MAILTO:team@angel.me" target="_blank"><i class="fa fa-envelope"></i></a>
			</li>
		</ul>
	</div>

	<div class="bottom">
		<p>Made with <i class="fa fa-heart" aria-hidden="true"></i> by Angel.me</p>
	</div>

</footer>