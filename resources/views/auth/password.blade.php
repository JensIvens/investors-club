@extends('layouts.app')

@section('content')
<section class="authenticate login">
    <div class="split left">
        <img src="/images/investorsclub_orange.png" alt="logo investors club">
    </div>
    <div class="split right">
        <section class="form">
        <h1>Password</h1>
        <p>Hey {{ ucfirst(Auth::user()->firstname) }}. Geef een passwoord in, zo kan je zowel inloggen met je Facebook account <em>als met jouw emailadres.</em></p>

    
        {!! Form::open(array('url' => '/setpassword', 'method' => 'POST'))!!}
            {{ csrf_field() }}
            
            {!! Form::label('firstname', 'Voornaam', ['class' => '']) !!}
            {!! Form::text('firstname', ucfirst(Auth::user()->firstname), ['class' => "{{ $errors->has('firstname') ? ' has-error' : '' }}", 'placeholder' => 'Voornaam', 'id' => 'firstname', 'required']) !!}

            {!! Form::label('lastname', 'Achternaam', ['class' => '']) !!}
            {!! Form::text('lastname', ucfirst(Auth::user()->lastname), ['class' => "{{ $errors->has('lastname') ? ' has-error' : '' }}", 'placeholder' => 'Achternaam', 'id' => 'lastname', 'required']) !!}

            {!! Form::label('email', 'Email:', ['class' => '']) !!}
            {!! Form::email('email', Auth::user()->email, ['class' => "{{ $errors->has('email') ? ' has-error' : '' }}", 'placeholder' => 'Emailadres', 'id' => 'email', 'required']) !!}

            {!! Form::label('password', 'Password', ['class' => '']) !!}
            {!! Form::password('password', ['class' => "{{ $errors->has('password') ? ' has-error' : '' }}", 'placeholder' => 'Wachtwoord', 'id' => 'password', 'required']) !!}

            {!! Form::label('password_confirmation', 'Bevestig password', ['class' => '']) !!}
            {!! Form::password('password_confirmation', ['class' => "{{ $errors->has('password_confirmation') ? ' has-error' : '' }}", 'placeholder' => 'Bevestig wachtwoord', 'id' => 'password-confirm', "required"]) !!}
                @if ($errors->any())
                    <span class="help-block">
                        <strong>{{ $errors->first() }}</strong>
                    </span>
                @endif
                

            {!! Form::submit('Voila!', ['class' => 'btn btn-default-inverse'])!!}
        {!! Form::close() !!}
        </section>
    </div>
</section>

@endsection