@extends('layouts.app')

@section('content')
<section class="authenticate login">
    <div class="split left">
        <img src="/images/investorsclub_orange.png" alt="logo investors club">
    </div>
    <div class="split right">
        <section class="form">
        <h1>Inloggen</h1>
        <p>Log in, beheer je profiel, creëer <em>een investeerdersprofiel</em></p>
        
        <a href="{{url('/auth/facebook')}}" class="btn btn-default-inverse btn-facebook"><i class="fa fa-facebook"></i>Login met Facebook</a>

        <hr>
        
        <p>Of login met je emailadres:</p>
        {!! Form::open(array('url' => '/login', 'method' => 'POST'))!!}
        {{ csrf_field() }}
        {!! Form::email('email', null, ['class' => "{{ $errors->has('email') ? ' has-error' : '' }}", 'placeholder' => 'Emailadres']) !!}
        @if ($errors->has('email'))
            <span class="help-block">
                <strong>{{ $errors->first('email') }}</strong>
            </span>
        @endif
        {!! Form::password('password', ['class' => "{{ $errors->has('password') ? ' has-error' : '' }}", 'placeholder' => 'Wachtwoord'])!!}
        @if ($errors->has('password'))
            <span class="help-block">
                <strong>{{ $errors->first('password') }}</strong>
            </span>
        @endif
        {!! Form::label('remember', 'Remember me') !!}
        {!! Form::checkbox('remember', 'remember', true) !!}
        <div class="password-reset"><a href="{{ url('/password/reset') }}">wachtwoord vergeten?</a></div>
            {!! Form::submit('Inloggen', ['class' => 'btn btn-default-inverse'])!!}
      {!! Form::close() !!}
        <p>Heb je nog geen account? <br> Geen probleem! Maak er <a href="/register">hier</a> eentje aan.</p>
        </section>
    </div>
</section>

@endsection