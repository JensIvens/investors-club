@extends('layouts.app')

@section('content')
<section class="authenticate register">
    <div class="split left">
        <img src="/images/investorsclub_orange.png" alt="logo investors club">
    </div>
    <div class="split right">
    <section class="form">
        <h1>Word lid!</h1>
        <p>Ben je klaar om de wereld mee te veranderen? Maak snel je account aan en vind interessante investeringsmogelijkheden.</p>

        <a href="{{url('/auth/facebook')}}" class="btn btn-default-inverse btn-facebook"><i class="fa fa-facebook"></i>Registreer met Facebook</a>
        
        <hr>

        <p>Of registreer je met jouw emailadres:</p>
            
        {!! Form::open(array('url' => '/register', 'method' => 'POST'))!!}
            {{ csrf_field() }}
            {!! Form::text('firstname', null, ['class' => "{{ $errors->has('firstname') ? ' has-error' : '' }}", 'placeholder' => 'Voornaam', 'id' => 'firstname']) !!}
                @if ($errors->has('firstname'))
                    <span class="help-block">
                        <strong>{{ $errors->first('firstname') }}</strong>
                    </span>
                @endif

            {!! Form::text('lastname', null, ['class' => "{{ $errors->has('lastname') ? ' has-error' : '' }}", 'placeholder' => 'Achternaam', 'id' => 'lastname']) !!}
                @if ($errors->has('lastname'))
                    <span class="help-block">
                        <strong>{{ $errors->first('lastname') }}</strong>
                    </span>
                @endif

            {!! Form::email('email', null, ['class' => "{{ $errors->has('email') ? ' has-error' : '' }}", 'placeholder' => 'Emailadres', 'id' => 'email']) !!}
                @if ($errors->has('email'))
                    <span class="help-block">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
                @endif

            {!! Form::password('password', ['class' => "{{ $errors->has('password') ? ' has-error' : '' }}", 'placeholder' => 'Wachtwoord', 'id' => 'password']) !!}
                @if ($errors->has('password'))
                    <span class="help-block">
                        <strong>{{ $errors->first('password') }}</strong>
                    </span>
                @endif

            {!! Form::password('password_confirmation', ['class' => "{{ $errors->has('password_confirmation') ? ' has-error' : '' }}", 'placeholder' => 'Bevestig wachtwoord', 'id' => 'password-confirm']) !!}
                @if ($errors->has('password_confirmation'))
                    <span class="help-block">
                        <strong>{{ $errors->first('password_confirmation') }}</strong>
                    </span>
                @endif

            {!! Form::submit('Aanmelden', ['class' => 'btn btn-default-inverse'])!!}
        {!! Form::close() !!}

        <p>Heb je al een account? <br> Dan kan je <a href="/login">hier inloggen</a>.</p>

    </section>
    </div>
    </section>
@endsection