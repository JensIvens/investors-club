@extends('layouts.app')

@section('content')
<header class="front-header clearfix" style="background-image: url('images/main-image.jpg')">
    <div class="clearfix">
        <h1 class="call-to-shift">Tax Shelter</h1>
        <div class="action-bar clearfix">
            <ul class="">
                <li><a href="#intro">Meer info</a></li>
                <li><a href="{{url('/register')}}">Word lid!</a></li>
            </ul>
        </div>
    </div>
</header>
<div class="content">
    <div class="clearfix">
        <div id="intro">
            <section class="split shift-brand">
                <img src="images/investorsclub.png" alt="">
            </section>
            <section class="split storytext">
                <h2>Tax Shelter</h2>
                <p><strong>De overheid geeft particuliere investeerders een mooi belastingsvoordeel!</strong></p>
                <p>Voor ondernemingen is het niet altijd even makkelijk om kapitaal te verzamelen. Om het kapitaal van particulieren te mobiliseren heeft de overheid een fiscale gunstmaatregel in het leven geroepen.
                </p>
                <p>Wanneer je nu als particulier gaat investeren in kleine vennootschappen geniet je een belastingsvermindering van 30% van het geïnvesteerde bedrag. Investeren in micro-ondernemingen is zelfs nog aantrekkelijker, je krijgt 45% van het geïnvesteerde bedrag terug via een belastingsvermindering op jouw personenbelasting.
                </p>
                <p><i>De Tax Shelter is enkel geldig voor inwoners van België. Personen die in een ander land belastingen betalen kunnen ook lid worden van de Angel.me Investors Club, maar kunnen niet genieten van het belastingsvoordeel.</i></p>
            </section>
        </div>
    </div>
    <div id="howto">
        <h2>Hoe werkt het? <span class="icon fa fa-credit-card"></span></h2>
    </div>
    <div id="goal">
        <section class="split left">
            <h2><strong>Tax Shelter</strong> in het kort..</h2>
            <p class="text-center">Onderaan vind je een uitgebreide versie<i class="fa fa-arrow-right"></i></p>
        </section>
        <section class="split right">
            <ul class="safety-measures">
                <li><span class="circle">1</span>
                    <p>Je investeert persoonlijk kapitaal in een jong bedrijf (maximum 4 jaar na oprichting) in ruil voor aandelen.</p>
                </li>
                <li><span class="circle">2</span>
                    <p>Je kan 30% of zelfs 45% eenmalig recupereren via een vermindering op jouw personenbelasting.
                    </p>
                </li>
                <li><span class="circle">3</span>
                    <p>Je dient de aandelen minstens 4 jaar in jouw bezit te houden, anders vervalt je belastingsvoordeel.
                    </p>
                </li>
                <li><span class="circle">4</span>
                    <p>Je kan jaarlijks maximaal €100.000,- investeren met de Tax Shelter. Uiteraard kan je meer investeren, maar op de meerwaarde krijg je geen belastingsvoordeel.
                    </p>
                </li>
                <li><span class="circle">5</span>
                    <p>Je kan maximaal €250.000,- investeren met de Tax Shelter (gespreid in de tijd). Uiteraard kan je meer investeren, maar op de meerwaarde krijg je geen belastingsvoordeel.
                    </p>
                </li>
                <li><span class="circle">6</span>
                    <p>Je mag maximaal 30% van de aandelen in je bezit krijgen van een bedrijf. Indien je toch meer aandelen zou kopen, dan kan je enkel gebruik maken van de Tax Shelter op het bedrag voor die 30%.
                    </p>
                </li>
            </ul>
        </section>
    </div>

    <div id="howto">
        <div class="howto huren">
            <section class="left split">
                <h3><strong>Tax Shelter</strong><br>in detail...<i class="fa fa-arrow-right"></i></h3>
            </section>
            <section class="right split">                   
                <div id="accordion">
                  <h3>De Tax Shelter in het kort:</h3>
                  <div>
                    <p>
                        Jonge ondernemingen kunnen moeilijkheden ondervinden bij het ophalen van kapitaal. Door het toekennen van een aantrekkelijke financiële gunstmaatregel wil de overheid particulieren aanzetten om hun privé-kapitaal te mobiliseren. 
                        </p>
                        <p>Zo kunnen investeerders nu genieten van een belastingsvermindering van 30% op het geïnvesteerde kapitaal bij een investering in een kleine vennootschap. Bij een investering in een micro-vennootschap krijgt de investeerder zelfs 45% terug van zijn geïnvesteerde kapitaal, ook via een belastingsvermindering.
                    </p>
                  </div>
                  <h3>Hoe werkt het?</h3>
                  <div>
                    <p>
                        1 - Je schrijft je als investeerder in op nieuwe aandelen van een startende of jonge onderneming (bijvoorbeeld naar aanleiding van een kapitaalsverhoging)
                    </p>
                    <p>
                        2 - De kapitaalsverhoging wordt bij de notaris geregistreerd en je bent officiëel aandeelhouder in de onderneming.
                    </p>
                    <p>
                        3 - Een aandeelhoudersovereenkomst wordt opgesteld en u ontvangt een bewijs van uw investering.
                    </p>
                    <p>
                        4 - Geef uw investering aan in uw personenbelasting en geniet van uw belastingvermindering.
                    </p>
                  </div>
                  <h3>Wie komt in aanmerking?</h3>
                  <div>
                    <p>
                        Startende ondernemingen:
                    </p>
                    <p>
                        De maatregel richt zich tot jonge ondernemingen (opgericht na 1 januari 2013) die gedurende twee opeenvolgende boekjaren niet meer dan 1 van volgende criteria overschreden:

                        - jaargemiddelde van het personeelsbestand tot 50 werknemers<br>
                        - jaaromzet is niet hoger dan €9.000.000,- (excl. BTW)<br>
                        - balanstotaal is niet hoger is dan €4.500.000,- (excl. BTW)<br>

                        Microvennootschappen zijn vennootschappen die geen dochtervennootschap of moedervennootschap zijn en die gedurende twee opeenvolgende boekjaren niet meer dan één van de volgende criteria overschreden:

                        - jaargemiddelde van het personeelsbestand tot 10 werknemers<br>
                        - jaaromzet is niet hoger dan €700.000,- (Excl. BTW)<br>
                        - balanstotaal is niet hoger is dan €350.000,- (excl. BTW)<br>
                    </p>
                    <p>
                        Investeerders:
                    </p>
                    <p>
                        Investeerders zijn alle natuurlijke personen, behalve:<br>
                        - de zaakvoerder of bestuurder van de startende vennootschap<br>
                        - of die personen die onrechtstreeks een functie van bedrijfsleider uitoefenen als vaste vertegenwoordiger van een andere vennootschap of door tussenkomst van een andere vennootschap waarvan ze aandeelhouders zijn.
                    </p>
                  </div>
                  <h3>Bedrag van fiscaal voordeel</h3>
                  <div>
                    <p>
                        Bij een investering in een kleine vennootschap:<br> 30% belastingvermindering van het geïnvesteerde bedrag
                    </p>
                    <p>
                        Bij een investering in een microvennootschap:<br>  45% belastingvermindering van het geïnvesteerde bedrag
                    </p>
                    <p>
                        En daarbovenop:<br>
                        Verlaagde roerende voorheffing op dividenden<br>
                        Vrijgestelde meerwaarde op aandelen
                    </p>
                  </div>
                  <h3>Voorwaarden </h3>
                  <div>
                    <p>
                        Enkel voor startende vennootschappen (bij oprichting of kapitaalverhoging binnen 4 jaar)
                    </p>
                    <p>
                        De investeerder:<br>
                        - De investeerder moet de aandelen 4 jaar aanhouden. Zoniet wordt de belastingvermindering pro rata teruggenomen.<br>
                        - Maximaal €250.000,- per vennootschap<br>
                        - Maximaal €100.000,- per investeerder per jaar<br>
                        - Een maximale participatie van 30% in de vennootschap<br>
                        - De aandelen moeten volledig volstort (lees: betaald) zijn
                    </p>
                    <p>
                        De vennootschap:
                        - Geen dividenden hebben uitgekeerd of kapitaalvermindering doorgevoerd<br>
                        - Geen management- of bestuurdersvennootschap zijn*<br>
                        - Geen beleggings-, thesaurie- of financieringsvennootschap zijn*<br>
                        - Geen verhuur, verwerving, beheer, … van vastgoed als voornaamste activiteit hebben*<br>
                        - Geen beursgenoteerde vennootschap zijn <br>
                        - Geen onderneming in moeilijkheden zijn<br>
                        - De vennootschap mag niet zijn opgericht in het kader van een fusie of splitsing van vennootschappen<br>
                        - De verkregen investering niet gebruiken voor de uitkering van dividenden, de overname van aandelen of het verstrekken van leningen*<br>
                    </p>
                    <p>
                        *Aan deze voorwaarden moet gedurende de 4 jaar waarin de investeerder de aandelen aanhoudt, worden voldaan.
                    </p>
                  </div>
                  <h3>Meer informatie </h3>
                  <div>
                    <p>
                        Voor meer informatie kan je altijd terecht op <a href="http://financien.belgium.be/nl/particulieren/belastingvoordelen/tax-shelter-%E2%80%93-investeren-een-startende-onderneming">de website van de Federale Overheidsdienst: Financiën.</a>
                    </p>
                    <p>
                        Je kan uiteraard ook altijd ons contacteren bij verdere vragen!
                    </p>
                  </div>
                  <h3>Wat als het foutloopt? </h3>
                  <div>
                    <p>
                        Investeren in jonge bedrijven houdt altijd een risico in. Het is belangrijk om vooraf goede afspraken te maken tussen de ondernemer, de oude aandeelhouder(s) en de nieuwe aandeelhouder(s). Zaken die best voorafgaandelijk worden besproken zijn de rechten en verplichtingen van de zaakvoerders/bestuurders en de aandeelhouders. Daarnaast zijn ook duidelijke afspraken betreffende een eventuele overdracht van aandelen in de toekomst aan te raden. In samenwerking met onze partners, begeleiden wij je graag bij de opmaak van de juiste contractuele documentatie.
                    </p>
                  </div>
                </div>
            </section>
        </div>

</div>
@endsection
@section('scripts')
    <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>

    <script>
    
        $( function() {
            $( "#accordion" ).accordion({
                heightStyle: "content"
            });
        });

      </script>
@endsection