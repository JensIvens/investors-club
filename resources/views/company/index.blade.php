@extends('layouts.app')

@section('content')

@include("partials.dashboard_navigation", ['active' => 'company'])
    <section class="user-page company clearfix">
        <section class="dashboard_content">
        	<div class="row">

				<div class="col-md-6">
        			<a href="{{ url('company/add') }}" class="btn btn-default">Add company</a>
        		</div>

        	</div>
        	<div class="row">
        		@foreach($companies as $company)
					<div class="col-md-6">
						<div class="content">
							<a href="{{url('/company/'. $company->id)}}">
							<i class="fa fa-building-o" aria-hidden="true"></i>
							<div class="details">
								<p class="title">{{ ucfirst($company->name) }}</p>
								<p>Needs funding? {{ucfirst($company->funding) }}</p> 
							</div>
						</a>
					</div>
				</div>
        		@endforeach
			</div>
        </section>
    </section>
@endsection