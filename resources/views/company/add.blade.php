@extends('layouts.app')

@section('content')

@include("partials.dashboard_navigation", ['active' => 'company'])
    <section class="user-page clearfix">
        <section class="dashboard_content">
           	<section class="dashboard_content_item">  
                <span class="dashboard_title_area">Basic info <i class="fa fa-angle-down pull-right" aria-hidden="true"></i></span>
                {!! Form::open(array('url' => '/dashboard/companies/add', 'method' => 'POST'))!!}
                {{ csrf_field() }}
                <div class="row">
                    <div class="clearfix form-group col-md-6">
                        {!! Form::label('name', 'Company name:', ['class' => '']) !!}
                        {!! Form::text('name', '', ['class' => 'form-control', 'placeholder' => 'Company name', 'required']) !!}
                    </div>
                    <div class="clearfix form-group col-md-6">
                        {!! Form::label('website', 'Website:', ['class' => '']) !!}
                        {!! Form::text('website', '', ['class' => 'form-control', 'placeholder' => 'Website']) !!}
                    </div>
                </div>
                <div class="row">
                    <div class="clearfix form-group col-md-6">
                        {!! Form::label('bio', 'Describe your company in 1 sentence:', ['class' => '']) !!}
                        {!! Form::text('bio', '', ['class' => 'form-control', 'placeholder' => 'Ex: We are the AirBnb for XYZ']) !!}
                    </div>
                    <div class="clearfix form-group col-md-6">
                        {!! Form::label('email', 'Email:', ['class' => '']) !!}
                        {!! Form::email('email', '', ['class' => 'form-control', 'placeholder' => 'Email:', 'required']) !!}
                    </div>
                </div>
                <div class="row">
                    <div class="clearfix form-group col-md-6">
                        {!! Form::label('phonenumber', 'Phone number:', ['class' => '']) !!}
                        {!! Form::text('phonenumber', '', ['class' => 'form-control', 'placeholder' => 'Phone number:']) !!}
                    </div>
                    <div class="clearfix form-group col-md-6">
                        {!! Form::label('location', 'Where are you located?', ['class' => '']) !!}
                        {!! Form::text('location', '', ['class' => 'form-control', 'placeholder' => 'Location:']) !!}
                    </div>
                </div>

            </section>
            <section id="edit-profile" class="dashboard_content_item">  
            <span class="dashboard_title_area">Funding <i class="fa fa-angle-down pull-right" aria-hidden="true"></i></span>
                <div class="row">
                    <div class="clearfix form-group col-md-6">
                        {!! Form::label('funding', 'Are you looking for funding?', ['class' => '']) !!}
                        {!! Form::select('funding', ['yes' => 'Yes, we want to raise money', 'no' => "No, we don't have the intention to raise money"], null, ['placeholder' => 'Are you looking for funding?', 'class' => 'form-control funding' , 'id' => 'selectfunding']); !!}
                    </div>
                    <div class="clearfix form-group col-md-6 amount">
                        {!! Form::label('amount', 'How much money do you want to raise?', ['class' => '']) !!}
                        {!! Form::select('amount', ['100K' => '- € 100.000,-', '250K' => "€ 100.000 - € 250.000", '500K' => '€ 250.000 - € 500.000', '1M' => '€ 500.000 - € 1.000.000', '+1M' => '+ €1.000.000'], null, ['placeholder' => 'How much money do you want to raise?', 'class' => 'form-control']); !!}
                    </div>
                </div>
            </section>
            <section class="dashboard_content_item">
                {!! Form::submit('Save!', ['class' => 'btn btn-default pull-right'])!!}
            </section>
            {!! Form::close() !!}
	</section>
</section>
@endsection