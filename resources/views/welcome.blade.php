@extends('layouts.app')

@section('content')
<div class="container">

    <h1>Welcome, {{ ucfirst(Auth::User()->firstname) }}</h1>
    <h3>Can you tell us a little more <i>about yourself?</i></h3>
    
    <p>To give you the best experience, we'd like to know if you're an investor, an entrepreneur or both?</p>
    <ul>
        <li>
            <img src="/images/placeholders/user-default.png" alt="Placeholder">
            <a href="{{url('welcome/investor') }}" class="btn btn-default-inverse">I'm an investor</a>
        </li>
        <li>
            <img src="/images/placeholders/user-default.png" alt="Placeholder">
            <a href="{{url('welcome/entrepreneur') }}" class="btn btn-default-inverse">I'm an entrepreneur</a>
        </li>
        <li>
            <img src="/images/placeholders/user-default.png" alt="Placeholder">
            <a href="{{url('dashboard') }}" class="btn btn-default-inverse">Both investor & entrepreneur</a>
        </li>
    </ul>   

</div>
<section class="banner" id="share">
  
    <h2>#INVESTORSCLUB <i class="fa fa-retweet" aria-hidden="true"></i></h2>

    <div class="row">
        <div class="col-md-6 color">
            
            <h3>Invite your friends</h3>
            <p>Invest together with your friends, colleagues or business partners and invite them to join</p>

            {!! Form::open(array('url' => '/welcome', 'method' => 'POST', 'id' => 'submitinviteform'))!!}
                {{ csrf_field() }}

                {!! Form::email('email', null, ['class' => "input", 'placeholder' => 'Emailadres']) !!}
                @if ($errors->has('email'))
                    <span class="help-block">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
                @endif

                {!! Form::submit('Invite!', ['class' => 'btn btn-default-inverse'])!!}

            {!! Form::close() !!}
        
        </div>
        <div class="col-md-6">
        
            <h3>Share</h3>
            <p>Help us get some traction and share your membership with your social friends</p>

            @include('partials.share', ['url' => 'http://investorsclub.angel.me/'])

        </div>
    </div>
</section>
@endsection
@section('scripts')
<script>

    var popupSize = {
        width: 780,
        height: 550
    };

    $(document).on('click', '.social-buttons > a', function(e){

        var
            verticalPos = Math.floor(($(window).width() - popupSize.width) / 2),
            horizontalPos = Math.floor(($(window).height() - popupSize.height) / 2);

        var popup = window.open($(this).prop('href'), 'social',
            'width='+popupSize.width+',height='+popupSize.height+
            ',left='+verticalPos+',top='+horizontalPos+
            ',location=0,menubar=0,toolbar=0,status=0,scrollbars=1,resizable=1');

        if (popup) {
            popup.focus();
            e.preventDefault();
        }

    });
</script>
@endsection
