<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/



/*
|--------------------------------------------------------------------------
| Homepage
|--------------------------------------------------------------------------
|
| Welcome stranger, this is the homepage!
|
*/
Route::group(['prefix' => LaravelLocalization::setLocale(),'middleware' => [ 'localeSessionRedirect', 'localizationRedirect' ]],function(){

    /** ADD ALL LOCALIZED ROUTES INSIDE THIS GROUP **/
	Route::get('/', 'HomeController@homepage');
	Route::post('/', 'HomeController@register');
	Route::get('/invest', 'HomeController@invest');
	Route::get('/taxshelter', 'HomeController@taxshelter');
	Route::get('/companies', 'HomeController@companies');

});



/*
|--------------------------------------------------------------------------
| Authentication
|--------------------------------------------------------------------------
|
| Login, register, forgot password dude? All the routes can be find here, 
| 100% automated by Laravel itself!
|
*/
Auth::routes();
Route::get('/auth/facebook', 'Auth\RegisterController@redirectToProvider');
Route::get('/auth/facebook/callback', 'Auth\RegisterController@handleProviderCallback');

Route::get('/angelme', 'UserController@angelme');

/*
|--------------------------------------------------------------------------
| User must be logged in!
|--------------------------------------------------------------------------
|
| If you want to see one of these routes, the user must be logged in.
| Otherwise he's directed to the homepage.
|
*/
Route::group(['middleware' => 'auth'], function () {

	// REGISTERED USER
	Route::get('/setpassword', 'UserController@password');
	Route::post('/setpassword', 'UserController@setpassword');

	// STATIC USER PAGES
	Route::get('/welcome', 'UserController@welcome');
	Route::get('/dashboard', 'UserController@index');

	// INVITE FRIENDS
	Route::post('/welcome', 'UserController@inviteFriend');

	// USER PROFILE & INVESTORSPROFILE
	Route::get('/dashboard/profile', 'UserController@profile');
	Route::post('/dashboard/profile', 'UserController@update');

	Route::get('/dashboard/investorsprofile', 'UserController@investorsprofile');
	Route::post('/dashboard/investorsprofile', 'UserController@updateinvestorsprofile');

	// COMPANIES: OVERVIEW, ADD, EDIT
	Route::get('/dashboard/companies', 'CompanyController@index');

	Route::get('/dashboard/companies/add', 'CompanyController@add');
	Route::post('/dashboard/companies/add', 'CompanyController@create');

	Route::get('/company/{id}', 'CompanyController@details');
	Route::post('/company/{id}', 'CompanyController@edit');

	// ADD PROFILE PICTURE
	Route::post('/users/addpicture', 'UserController@addPicture');

	// VERIFY EMAIL
	Route::get('/verify/{emailkey}', 'UserController@addEmailVerification');
	Route::get('/resendverifyemail', 'UserController@sendEmailVerification');

	Route::get('/admin', 'UserController@admin');

});



/*
|--------------------------------------------------------------------------
| Hey worker, send out those emails
|--------------------------------------------------------------------------
|
| 
| Otherwise he's directed to the homepage.
|
*/
Route::post('/worker/queue', 'WorkerController@queue');
Route::post('/worker/schedule', "WorkerController@schedule");