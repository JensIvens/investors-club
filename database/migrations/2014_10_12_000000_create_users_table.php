<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('firstname');
            $table->string('lastname');
            $table->string('email')->unique();
            $table->string('password');
            $table->string('facebook_id')->unique();
            $table->string('public');
            $table->integer('image_id')->nullable();
            $table->string('bio')->nullable();
            $table->string('phonenumber')->nullable();
            $table->string('street')->nullable();
            $table->string('number')->nullable();
            $table->string('postalcode')->nullable();
            $table->string('city')->nullable();
            $table->string('country')->nullable();
            $table->string('entrepreneur')->default(0);
            $table->string('investor')->default(0);
            $table->string('email_verification')->nullable();
            $table->string('admin');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('users');
    }
}
