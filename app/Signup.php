<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Signup extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'signups';
    protected $fillable = ['email'];

}
