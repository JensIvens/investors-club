<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Support\Facades\Auth;
use App\User;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\RegistersUsers;
use App\Mail\Registered;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Investorsprofile;
use Socialite;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    protected $redirectTo = '/welcome';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'firstname' => 'required|max:25',
            'lastname' => 'required|max:25',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|min:6|confirmed',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
        Mail::to($data['email'])->queue(new Registered());

        return User::create([
            'firstname' => $data['firstname'],
            'lastname' => $data['lastname'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
            'public' => false,
            'admin' => "0",
        ]);

    }

    /**
     * Redirect the user to the GitHub authentication page.
     *
     * @return Response
     */
    public function redirectToProvider()
    {
        return Socialite::driver('facebook')->redirect();
    }

    /**
     * Obtain the user information from GitHub.
     *
     * @return Response
     */
    public function handleProviderCallback()
    {

        try {

            $socialUser = Socialite::driver('facebook')->user();
            
        } catch (Exception $e) {
            return redirect('/');
        }

        $user = User::where('facebook_id', '=', $socialUser->getId())->first();

        if(!$user)
        {
            $name = $socialUser->getName();

            $parts = explode(" ", $name);
            $lastname = array_pop($parts);
            $firstname = implode(" ", $parts);


            $user = User::create([
                    'facebook_id' => $socialUser->getId(),
                    'firstname' => $firstname,
                    'lastname' => $lastname,
                    'email' => $socialUser->getEmail(),
                    'password' => '',
                    'email_verification' => 1,
                    'public' => false,
                    'admin' => "1",
                ]);

            auth()->login($user);
            return redirect('/setpassword');
        }

        auth()->login($user);

        return redirect('/dashboard');
    }
}
