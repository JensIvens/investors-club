<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\User;
use Auth;
use Illuminate\Support\Facades\DB;
use App\Company;


class CompanyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $companies = Company::where('user_id', '=', Auth::user()->id)->get();


        return view('company.index', compact('companies'));
    }

    public function add()
    {
        //

        return view('company.add');
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        //

        $company = new Company;

        $company->user_id = Auth::user()->id;
        $company->name = $request->name;
        $company->bio = $request->bio;
        $company->email = $request->email;
        $company->website = $request->website;
        $company->phonenumber = $request->phonenumber;
        $company->location = $request->location;
        $company->funding = $request->funding;
        $company->amount = $request->amount;


        $company->save();

        return view('companies.index');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function details($id)
    {
        //
        $company = Company::findorfail($id);
        if(Auth::user()->id == $company->user_id)
        {
            return view('company.details', compact('company'));
        }   
        else
        {
            $error = "Sorry, you don't have access to that page";
            return view('dashboard', compact("error"));
        }

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        //
        $company = Company::where('id', '=', $id)->first();

        $company->name = $request->name;
        $company->bio = $request->bio;
        $company->email = $request->email;
        $company->website = $request->website;
        $company->phonenumber = $request->phonenumber;
        $company->location = $request->location;
        $company->funding = $request->funding;
        $company->amount = $request->amount;

        $company->save();

        return back();

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
