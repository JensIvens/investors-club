<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\User;
use Auth;
use App\Signup;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */

/*    public function __construct()
    {
        $this->middleware('auth');
    }*/

    public function homepage()
    {

        $investors = User::where('public', '=', 'yes')->get();

        return view('index', compact('investors'));
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function taxshelter()
    {
        return view('taxshelter');
    }

    public function invest()
    {
        return view('invest');
    }

    public function companies()
    {
        return view('companies');
    }

    public function update(Request $request)
    {

    }
}
