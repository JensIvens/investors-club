<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\File\UploadedFile;

use App\Http\Requests;
use App\User;
use Auth;
use App\Userphoto;
use App\Investorsprofile;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use App\Mail\InviteFriends;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return view('dashboard');
    }

    public function profile()
    {
        //
        return view('user.profile');
    }

    public function investorsprofile()
    {

        $investorsprofile = Investorsprofile::firstOrCreate(['user_id' => Auth::user()->id]);

        return view('user.investorsprofile', compact('investorsprofile'));
    }



    public function makePhoto(UploadedFile $file)
    {

      return Userphoto::named($file->getClientOriginalName())->move($file);
    
    }

    public function deletePhoto()
    {
        return Userphoto::named($file->getClientOriginalName())->move($file);
    }
    public function addPicture(Request $request)
    {

        if(Auth::user()->imageid != null)
        {
            Auth::user()->deletePhoto();
        }
        

        // Validate the file
        $this->validate($request, ['profilepicture' => 'mimes:jpg,jpeg,png,bmp,gif']);

        $user = Auth::User();

        $userPhoto = $this->makePhoto($request->file('profilepicture'));

        $user->addPicture($userPhoto);
        $lastphoto = count($user->photos) -1;
        $user->update(array('image_id' => $user->photos[$lastphoto]->id));
            
        return back();

    }

    public function welcome()
    {

        return view('welcome');
    }


    public function sendEmailVerification()
    {
    
      return view('emails.verifyemail');
    
    }
   public function addEmailVerification($emailkey)
   {

        $user = Auth::User();
        
        if($emailkey == '4ng3lm31nvCl')
        {
           $user->email_verification = 1;
           $user->update();
        }

        return redirect('/dashboard');
    }

    public function inviteFriend(array $datastring)
    {
            
        $this->validate($request, [
            'email' => 'required|email|max:255|unique:users'
        ]);
            
        Mail::to($request['email'])->queue(new InviteFriends());
            
        return back();
    }


    public function admin()
    {
        //
        if(Auth::user()->admin == 1)
        {
            $users = DB::table('users')
                    ->orderBy('created_at', 'desc')
                    ->groupBy('created_at')
                    ->count();

            return view('admin.index', compact('users'));
        }
        else
        {
            return back();
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        //
        $user = Auth::User();
        $user->update($request->all());

        return back();

    }

    public function updateinvestorsprofile(Request $request)
    {
        //
        $investorsprofile = Investorsprofile::where('user_id', '=', Auth::user()->id)->first();
        $investorsprofile->user_id = Auth::user()->id;
        $investorsprofile->update($request->all());

        return back();

    }

    public function password()
    {
        return view('auth.password');
    }

    public function setpassword(Request $request)
    {

        if($request->password == $request->password_confirmation)
        {
            $user = Auth::user();
            $user->firstname = $request->firstname;
            $user->lastname = $request->lastname;
            $user->email = $request->email;
            $user->password = bcrypt($request->password);
            $user->save();

            return redirect('/welcome');
        }
        else 
        {
            return back()->withErrors(["Passwords don't match, try again"]);
        }
        

    }

    public function angelme()
    {
        return view('invest.overview');
    }
}
