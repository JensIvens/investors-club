<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
    //

    protected $table = 'companies';
    protected $fillable = [
    		'user_id', 'name', 'bio', 'email', 'website', 'phonenumber', 'location', 'funding', 'amount'
    ];

	


	// Relationships
	public function user() {
    
		return $this->belongsTo('App\Models\User');
  
	}


}
