<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'firstname', 'lastname', 'email', 'facebook_id', 'password', 'admin', 'public', 'image_id', 'bio', 'phonenumber', 'street', 'number', 'postalcode', 'city', 'country'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function photos() {
      
      return $this->hasMany('App\Userphoto');
    
    }

    public function image() {
    
      return $this->belongsTo('App\Userphoto');
    
    }

    public function addPicture(Userphoto $UserPhoto){
    
      return $this->photos()->save($UserPhoto);
    
    }
}
