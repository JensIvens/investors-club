<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Investorsprofile extends Model
{
    //

    protected $table = 'investorsprofiles';
    protected $fillable = ['user_id', 'experience', 'amount', 'location', 'risk', 'stage' ];

     // Relationships
	public function user() {
    
		return $this->belongsTo('App\Models\User');
  
	}


}
